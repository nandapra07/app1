'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class AddNew extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  AddNew.init({
    sn: DataTypes.STRING,
    HWBrand: DataTypes.STRING,
    Application: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'AddNew',
  });
  return AddNew;
};