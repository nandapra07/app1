var express = require('express');
var path = require('path');
const bodyParser = require('body-parser');

const userRouters = require('./src/domain/user/controller/http/userRoute');

const env = require('dotenv');
env.config();

var app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

global.rd = __dirname;

app.use(bodyParser.json());
app.set('json spaces', 2);
app.use(express.static(__dirname + '/public'));

app.use('/api', userRouters);

const PORT = process.env.PORT || 2700;
const APPLICATION_NAME =
  process.env.APPLICATION_NAME || 'app1';

app.listen(PORT, () => {
  console.log(
    `Server is listening on port ${PORT} with Application Name: ${APPLICATION_NAME}`
  );
});

