const ajv = require('ajv');
const UserRepository = require('../repository/userRepository');

class UserController {
    async register(req, res) {
        try {
            const userRepositoryInit = new userRepository();
            const schema = {
                type: "object",
                properties: {
                    name: {
                        type: "string",
                        minLength: 1
                    },
                    nik: {
                        type: "integer",
                        minLength: 1
                    },
                    email: {
                        type: "string",
                        pattern: "/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/"
                    },
                    password: {
                        type: "string",
                        minLength: 8,
                        maxLength: 8
                    },
                    company: {
                        type: "string",
                        minLength: 1
                    }
                },
                required: ["name", "nik", "email", "password", "company"],
                additionalProperties: false
            }
            const body = {
                name: req.body.name,
                nik: req.body.nik,
                email: req.body.email,
                password: req.body.password,
                company: req.body, company
            }
            const validateInput = ajv.compile(schema)
            const valid = validateInput(body)
            if (valid) {
                const registerUser = await userRepositoryInit.createUser(
                    name,
                    nik,
                    email,
                    password,
                    company
                );
                res.status(201).json({
                    message: "Success create user account",
                    data: registerUser,
                });
            } else {
                res.status(400).json(validateInput.errors);
            }
        } catch (error) {
            res.status(500).json(error);
        }
    }

    async login(req, res) {
        try {
            const userRepositoryInit = new UserRepository();
            const schema = {
                type: "object",
                properties: {
                    email: {
                        type: "string",
                        pattern: "/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/"
                    },
                    password: {
                        type: "string",
                        minLength: 8,
                        maxLength: 8
                    }
                },
                required: ["email", "password"],
                additionalProperties: false
            }
            const body = {
                email: req.body.email,
                password: req.body.password
            }
            const validateInput = ajv.compile(schema)
            const valid = validateInput(body)
            if (valid) {
                const loginUser = await userRepositoryInit.verifyAuth(
                    email,
                    password
                )
                res.status(200).json({
                    message: "Success login",
                    data: loginUser,
                });
            } else {
                res.status(400).json(validateInput.errors);
            }
        } catch (error) {
            res.status(500).json(error);
        }
    }
}

module.exports = UserController;