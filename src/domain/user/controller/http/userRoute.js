const express = require('express');
const userController = require('../../usecase/userUsecase');
const router = express.Router();

const UserController = new userController();

router.post('/login', UserController.login)
router.post('/register', UserController.register)

module.exports = router;