const db = require('../../../../models');

class UserRepository{
    async verifyAuth(email, token){
        try{
            const getVerify = await db.users.findOne({
                where: {
                    email: email,
                    password: password
                }
            })
            console.log(getVerify)
            return getVerify
        }catch(error){
            return error;
        }
    }

    async createUser(name, nik, email, password, company){
        try{
            const result = await db.users.create({
                name: name,
                nik: nik,
                email: email,
                password: password,
                company: company
            })
            console.log(result)
            return result;
        }catch(error){
            return error;
        }
    }
}

module.exports = UserRepository;